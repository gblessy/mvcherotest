package com.vladromanchenko.pageobjects;

import com.vladromanchenko.pagemodules.testConfigs.BaseTest;
import com.vladromanchenko.pageobjects.pages.TodoMVCPage;
import org.junit.Test;


/**
 * Created by v.romanchenko on 4/23/2016.
 */
public class TodoMVCCompletedFilterTest extends BaseTest {

    TodoMVCPage task = new TodoMVCPage();
    TodoMVCPage.Status ACTIVE = TodoMVCPage.Status.ACTIVE;
    TodoMVCPage.Status COMPLETED = TodoMVCPage.Status.COMPLETED;

    @Test
    public void testCreateAtCompletedFilter() {
        task.givenAtCompleted(ACTIVE, "a");

        task.add("b");
        task.assertNoVisibleTasks();
        task.assertItemsLeft(2);
    }

    @Test
    public void testEditAtCompletedFilter() {
        task.givenAtCompleted(COMPLETED, "a", "b");

        task.edit("b", "b edited");
        task.assertTasks("a", "b edited");
        task.assertItemsLeft(0);
    }

    @Test
    public void testDeleteAtCompletedFilter() {
        task.givenAtCompleted(COMPLETED, "a");

        task.delete("a");
        task.assertNoTasks();
    }

    @Test
    public void testCancelEditAtCompletedFilter() {
        task.givenAtCompleted(COMPLETED, "a", "b");

        task.cancelEdit("b", "b edited");
        task.assertTasks("a", "b");
        task.assertItemsLeft(0);
    }

    @Test
    public void testCompleteAllAtCompletedFilter() {
        task.givenAtCompleted(task.aTask(ACTIVE, "a"), task.aTask(COMPLETED, "b"), task.aTask(ACTIVE, "c"));

        task.toggleAll();
        task.assertTasks("a", "b", "c");
        task.assertItemsLeft(0);
    }

    @Test
    public void testReopenAllAtCompletedFilter() {
        task.givenAtCompleted(COMPLETED, "a", "b");

        task.toggleAll();
        task.assertNoVisibleTasks();
        task.assertItemsLeft(2);
    }

    @Test
    public void testEditClickOutsideAtCompleteFilter() {
        task.givenAtCompleted(COMPLETED, "a", "b");

        task.startEdit("a", "a edited");
        task.newtodo.click();
        task.assertTasks("a edited", "b");
        task.assertItemsLeft(0);
    }

    @Test
    public void testEditPressTabAtCompletedFilter() {
        task.givenAtCompleted(COMPLETED, "a");

        task.startEdit("a", "a edited").pressTab();
        task.assertTasks("a edited");
        task.assertItemsLeft(0);
    }

    @Test
    public void testDeleteByEmptyingTaskTextAtCompletedFilter() {
        task.givenAtCompleted(COMPLETED, "a");

        task.edit("a", "");
        task.assertNoVisibleTasks();
    }

    @Test
    public void testSwitchFromCompletedToActive() {
        task.givenAtCompleted(task.aTask(ACTIVE, "a"), task.aTask(COMPLETED, "b"));

        task.filterActive();
        task.assertVisibleTasks("a");
        task.assertItemsLeft(1);
    }
}