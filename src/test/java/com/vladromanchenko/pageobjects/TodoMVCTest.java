package com.vladromanchenko.pageobjects;


import com.vladromanchenko.pagemodules.testConfigs.BaseTest;
import com.vladromanchenko.pageobjects.pages.TodoMVCPage;
import org.junit.Test;

import static com.vladromanchenko.pagemodules.pages.TodoMVCGivens.givenAtAll;

public class TodoMVCTest extends BaseTest {

    TodoMVCPage task = new TodoMVCPage();
    TodoMVCPage.Status ACTIVE = TodoMVCPage.Status.ACTIVE;
    TodoMVCPage.Status COMPLETED = TodoMVCPage.Status.COMPLETED;

    @Test
    public void testTasksCommonFlow() {
        task.givenAtAll(ACTIVE, "a");
        task.edit("a", "edited a");
        task.toggle("edited a");
        task.assertTasks("edited a");

        task.filterActive();
        task.assertNoVisibleTasks();

        task.add("b");
        task.assertVisibleTasks("b");

        //complete all
        task.toggleAll();
        task.assertNoVisibleTasks();

        task.filterCompleted();
        task.assertVisibleTasks("edited a", "b");
        //reopen
        task.toggle("b");
        task.assertVisibleTasks("edited a");

        task.clearCompleted();
        task.assertNoVisibleTasks();

        task.filterAll();
        task.assertVisibleTasks("b");

        task.delete("b");
        task.assertNoTasks();
    }
}