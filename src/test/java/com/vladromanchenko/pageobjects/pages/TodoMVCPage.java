package com.vladromanchenko.pageobjects.pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.ArrayList;

import static com.codeborne.selenide.CollectionCondition.empty;
import static com.codeborne.selenide.CollectionCondition.exactTexts;
import static com.codeborne.selenide.Condition.cssClass;
import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.url;

/**
 * Created by v.romanchenko on 4/23/2016.
 */
public class TodoMVCPage {

    public ElementsCollection tasks = $$("#todo-list>li");
    public SelenideElement newtodo = $("#new-todo");

    @Step
    public void add(String... taskTexts) {
        for (String text : taskTexts) {
            newtodo.setValue(text).pressEnter();
        }
    }

    public SelenideElement startEdit(String oldTaskText, String newTaskText) {
        tasks.find(exactText(oldTaskText)).doubleClick();
        return tasks.find(cssClass("editing")).$(".edit").setValue(newTaskText);
    }

    @Step
    public void edit(String oldTaskText, String newTaskText) {
        startEdit(oldTaskText, newTaskText).pressEnter();
    }

    @Step
    public void cancelEdit(String oldTaskText, String newTaskText) {
        startEdit(oldTaskText, newTaskText).pressEscape();
    }

    @Step
    public void delete(String taskText) {
        tasks.find(exactText(taskText)).hover().$(".destroy").click();
    }

    @Step
    public void toggle(String taskText) {
        tasks.find(exactText(taskText)).$(".toggle").click();
    }

    @Step
    public void clearCompleted() {
        $("#clear-completed").click();
        $("#clear-completed").shouldNotBe(visible);
    }

    @Step
    public void toggleAll() {
        $("#toggle-all").click();
    }

    @Step
    public void assertTasks(String... taskTexts) {
        tasks.shouldHave(exactTexts(taskTexts));
    }

    @Step
    public void assertVisibleTasks(String... taskTexts) {
        tasks.filter(visible).shouldHave(exactTexts(taskTexts));
    }

    @Step
    public void assertNoTasks() {
        tasks.shouldBe(empty);
    }

    @Step
    public void assertNoVisibleTasks() {
        tasks.filter(visible).shouldBe(empty);
    }

    @Step
    public void filterAll() {
        $(By.linkText("All")).click();
    }

    @Step
    public void filterActive() {
        $(By.linkText("Active")).click();
    }

    @Step
    public void filterCompleted() {
        $(By.linkText("Completed")).click();
    }

    @Step
    public void assertItemsLeft(int count) {

        $("#todo-count>strong").shouldHave(exactText("" + count));
    }

    public void givenAtAll(Task... tasks) {
        ensurePageOpened();
        ArrayList<String> list = new ArrayList<String>();

        for (Task task : tasks) {
            list.add(task.toString());
        }

        String createTaskCommand = "localStorage.setItem(\"todos-troopjs\", \"[" + String.join(",", list) + "]\")";
        executeJavaScript(createTaskCommand);
        refresh();
    }

    public void givenAtActive(Task... tasks) {
        givenAtAll(tasks);
        filterActive();
    }

    public void givenAtCompleted(Task... tasks) {
        givenAtAll(tasks);
        filterCompleted();
    }

    public void givenAtAll(Status taskStatus, String... taskTexts) {
        givenAtAll(aTasks(taskStatus, taskTexts));
    }

    public void givenAtActive(Status taskStatus, String... taskTexts) {
        givenAtAll(aTasks(taskStatus, taskTexts));
        filterActive();
    }

    public void givenAtCompleted(Status taskStatus, String... taskTexts) {
        givenAtAll(aTasks(taskStatus, taskTexts));
        filterCompleted();
    }

    public Task aTask(Status taskStatus, String text) {
        return new Task(taskStatus, text);
    }

    public Task[] aTasks(Status taskStatus, String... text) {
        Task[] tasks = new Task[text.length];
        for (int i = 0; i < text.length; i++) {
            tasks[i] = aTask(taskStatus, text[i]);
        }
        return tasks;
    }

    public void ensurePageOpened() {
        if (!url().equals("https://todomvc4tasj.herokuapp.com/")) {
            open("https://todomvc4tasj.herokuapp.com/");
        }

    }

    public enum Status {
        ACTIVE("false"), COMPLETED("true");

        private String flag;

        Status(String flag) {
            this.flag = flag;
        }

        @Override
        public String toString() {
            return flag;
        }
    }

    public class Task {

        private String actualString = "";

        public Task(Status state, String text) {
            this.actualString = "{\\\"completed\\\":" + state + ", \\\"title\\\":\\\"" + text + "\\\"}";
        }

        @Override
        public String toString() {
            return actualString;
        }
    }

}
