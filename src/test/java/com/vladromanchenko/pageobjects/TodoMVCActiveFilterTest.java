package com.vladromanchenko.pageobjects;

import com.vladromanchenko.pageobjects.pages.TodoMVCPage;
import com.vladromanchenko.pageobjects.testConfigs.BaseTest;
import org.junit.Test;

import static com.vladromanchenko.pageobjects.pages.TodoMVCPage.Status.ACTIVE;
import static com.vladromanchenko.pageobjects.pages.TodoMVCPage.Status.COMPLETED;


/**
 * Created by v.romanchenko on 4/23/2016.
 */
public class TodoMVCActiveFilterTest extends BaseTest {
    TodoMVCPage task = new TodoMVCPage();

    @Test
    public void testEditAtActiveFilter() {
        task.givenAtActive(ACTIVE, "a");

        task.edit("a", "a edited");
        task.assertVisibleTasks("a edited");
        task.assertItemsLeft(1);
    }

    @Test
    public void testDeleteAtActiveFilter() {
        task.givenAtActive(ACTIVE, "a", "b");

        task.delete("b");
        task.assertTasks("a");
        task.assertItemsLeft(1);
    }

    @Test
    public void testCompleteAtActiveFilter() {
        task.givenAtActive(ACTIVE, "a", "b");

        task.toggle("a");
        task.assertVisibleTasks("b");
        task.assertItemsLeft(1);
    }

    @Test
    public void testClearCompletedAtActiveFilter() {
        task.givenAtActive(task.aTask(COMPLETED, "a"), task.aTask(ACTIVE, "b"));

        task.clearCompleted();
        task.assertVisibleTasks("b");
        task.assertItemsLeft(1);
    }

    @Test
    public void testReopenAllAtActiveFilter() {
        task.givenAtActive(COMPLETED, "a", "b", "c");

        task.toggleAll();
        task.assertTasks("a", "b", "c");
        task.assertItemsLeft(3);
    }

    @Test
    public void testCancelEditAtActiveFilter() {
        task.givenAtActive(ACTIVE, "a");

        task.cancelEdit("a", "a edited");
        task.assertVisibleTasks("a");
        task.assertItemsLeft(1);
    }

    @Test
    public void testEditClickOutsideAtActiveFilter() {
        task.givenAtActive(ACTIVE, "a");

        task.startEdit("a", "a edited");
        task.newtodo.click();
        task.assertTasks("a edited");
        task.assertItemsLeft(1);
    }

    @Test
    public void testEditPressTabAtActiveFilter() {
        task.givenAtActive(ACTIVE, "a");

        task.startEdit("a", "a edited").pressTab();
        task.assertTasks("a edited");
        task.assertItemsLeft(1);
    }

    @Test
    public void testDeleteByEmptyingTaskTextAtActiveFilter() {
        task.givenAtActive(ACTIVE, "a");

        task.edit("a", "");
        task.assertNoVisibleTasks();
    }

    @Test
    public void testSwitchFromActiveToAll() {
        task.givenAtActive(task.aTask(ACTIVE, "a"), task.aTask(COMPLETED, "b"));

        task.filterAll();
        task.assertTasks("a", "b");
        task.assertItemsLeft(1);
    }
}
