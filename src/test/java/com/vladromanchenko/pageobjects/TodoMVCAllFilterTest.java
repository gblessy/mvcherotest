package com.vladromanchenko.pageobjects;

import com.vladromanchenko.pagemodules.testConfigs.BaseTest;
import com.vladromanchenko.pageobjects.pages.TodoMVCPage;
import org.junit.Test;


/**
 * Created by v.romanchenko on 4/23/2016.
 */
public class TodoMVCAllFilterTest extends BaseTest {

    TodoMVCPage task = new TodoMVCPage();
    TodoMVCPage.Status ACTIVE = TodoMVCPage.Status.ACTIVE;
    TodoMVCPage.Status COMPLETED = TodoMVCPage.Status.COMPLETED;


    @Test
    public void testCompleteAllAtAllFilter() {
        task.givenAtAll(ACTIVE, "a", "b", "c");

        task.toggleAll();
        task.assertTasks("a", "b", "c");
        task.assertItemsLeft(0);
    }

    @Test
    public void testClearCompletedAtAllFilter() {
        task.givenAtAll(COMPLETED, "a", "b", "c");

        task.clearCompleted();
        task.assertNoTasks();
    }

    @Test
    public void testReopenAtAllFilter() {
        task.givenAtAll(COMPLETED, "a", "b", "c");

        task.toggle("a");
        task.filterActive();
        task.assertVisibleTasks("a");
        task.assertItemsLeft(1);
    }

    @Test
    public void testReopenAllAtAllFilter() {
        task.givenAtAll(COMPLETED, "a");

        task.toggleAll();
        task.assertTasks("a");
        task.assertItemsLeft(1);
    }

    @Test
    public void testCancelEditAtAllFilter() {
        task.givenAtAll(task.aTask(ACTIVE, "a"), task.aTask(COMPLETED, "b"));

        task.cancelEdit("a", "a edited");
        task.assertTasks("a", "b");
        task.assertItemsLeft(1);
    }

    @Test
    public void testEditClickOutsideAtAllFilter() {
        task.givenAtAll(task.aTask(ACTIVE, "a"), task.aTask(COMPLETED, "b"));

        task.startEdit("a", "a edited");
        task.newtodo.click();
        task.assertTasks("a edited", "b");
        task.assertItemsLeft(1);
    }

    @Test
    public void testEditPressTabAtAllFilter() {
        task.givenAtAll(task.aTask(ACTIVE, "a"), task.aTask(COMPLETED, "b"));

        task.startEdit("a", "a edited").pressTab();
        task.assertTasks("a edited", "b");
        task.assertItemsLeft(1);
    }

    @Test
    public void testDeleteByEmptyingTaskTextAtAllFilter() {
        task.givenAtAll(task.aTask(ACTIVE, "a"), task.aTask(COMPLETED, "b"));

        task.edit("a", "");
        task.assertTasks("b");
    }

    @Test
    public void testSwitchFromAllToCompletedFilter() {
        task.givenAtAll(task.aTask(COMPLETED, "a"), task.aTask(ACTIVE, "b"));

        task.filterCompleted();
        task.assertVisibleTasks("a");
        task.assertItemsLeft(1);
    }
}
