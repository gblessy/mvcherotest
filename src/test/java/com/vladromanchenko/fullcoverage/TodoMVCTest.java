package com.vladromanchenko.fullcoverage;


import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.vladromanchenko.fullcoverage.testConfigs.BaseTest;
import org.junit.Test;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.ArrayList;

import static com.codeborne.selenide.CollectionCondition.empty;
import static com.codeborne.selenide.CollectionCondition.exactTexts;
import static com.codeborne.selenide.Condition.cssClass;
import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.url;

public class TodoMVCTest extends BaseTest {

    Status ACTIVE = Status.ACTIVE;
    Status COMPLETED = Status.COMPLETED;

    @Test
    public void testTasksCommonFlow() {
        givenAtAll(ACTIVE, "a");
        edit("a", "edited a");
        toggle("edited a");
        assertTasks("edited a");

        filterActive();
        assertNoVisibleTasks();

        add("b");
        assertVisibleTasks("b");

        //complete all
        toggleAll();
        assertNoVisibleTasks();

        filterCompleted();
        assertVisibleTasks("edited a", "b");
        //reopen
        toggle("b");
        assertVisibleTasks("edited a");

        clearCompleted();
        assertNoVisibleTasks();

        filterAll();
        assertVisibleTasks("b");

        delete("b");
        assertNoTasks();
    }

    @Test
    public void testCompleteAllAtAllFilter() {
        givenAtAll(ACTIVE, "a", "b", "c");

        toggleAll();
        assertTasks("a", "b", "c");
        assertItemsLeft(0);
    }

    @Test
    public void testClearCompletedAtAllFilter() {
        givenAtAll(COMPLETED, "a", "b", "c");

        clearCompleted();
        assertNoTasks();
    }

    @Test
    public void testReopenAtAllFilter() {
        givenAtAll(COMPLETED, "a", "b", "c");

        toggle("a");
        filterActive();
        assertVisibleTasks("a");
        assertItemsLeft(1);
    }

    @Test
    public void testReopenAllAtAllFilter() {
        givenAtAll(COMPLETED, "a");

        toggleAll();
        assertTasks("a");
        assertItemsLeft(1);
    }

    @Test
    public void testCancelEditAtAllFilter() {
        givenAtAll(aTask(ACTIVE, "a"), aTask(COMPLETED, "b"));

        cancelEdit("a", "a edited");
        assertTasks("a", "b");
        assertItemsLeft(1);
    }

    @Test
    public void testEditClickOutsideAtAllFilter() {
        givenAtAll(aTask(ACTIVE, "a"), aTask(COMPLETED, "b"));

        startEdit("a", "a edited");
        newtodo.click();
        assertTasks("a edited", "b");
        assertItemsLeft(1);
    }

    @Test
    public void testEditPressTabAtAllFilter() {
        givenAtAll(aTask(ACTIVE, "a"), aTask(COMPLETED, "b"));

        startEdit("a", "a edited").pressTab();
        assertTasks("a edited", "b");
        assertItemsLeft(1);
    }

    @Test
    public void testDeleteByEmptyingTaskTextAtAllFilter() {
        givenAtAll(aTask(ACTIVE, "a"), aTask(COMPLETED, "b"));

        edit("a", "");
        assertTasks("b");
    }

    @Test
    public void testSwitchFromAllToCompletedFilter() {
        givenAtAll(aTask(COMPLETED, "a"), aTask(ACTIVE, "b"));

        filterCompleted();
        assertVisibleTasks("a");
        assertItemsLeft(1);
    }

    @Test
    public void testEditAtActiveFilter() {
        givenAtActive(ACTIVE, "a");

        edit("a", "a edited");
        assertVisibleTasks("a edited");
        assertItemsLeft(1);
    }

    @Test
    public void testDeleteAtActiveFilter() {
        givenAtActive(ACTIVE, "a", "b");

        delete("b");
        assertTasks("a");
        assertItemsLeft(1);
    }

    @Test
    public void testCompleteAtActiveFilter() {
        givenAtActive(ACTIVE, "a", "b");

        toggle("a");
        assertVisibleTasks("b");
        assertItemsLeft(1);
    }

    @Test
    public void testClearCompletedAtActiveFilter() {
        givenAtActive(aTask(COMPLETED, "a"), aTask(ACTIVE, "b"));

        clearCompleted();
        assertVisibleTasks("b");
        assertItemsLeft(1);
    }

    @Test
    public void testReopenAllAtActiveFilter() {
        givenAtActive(COMPLETED, "a", "b", "c");

        toggleAll();
        assertTasks("a", "b", "c");
        assertItemsLeft(3);
    }

    @Test
    public void testCancelEditAtActiveFilter() {
        givenAtActive(ACTIVE, "a");

        cancelEdit("a", "a edited");
        assertVisibleTasks("a");
        assertItemsLeft(1);
    }

    @Test
    public void testEditClickOutsideAtActiveFilter() {
        givenAtActive(ACTIVE, "a");

        startEdit("a", "a edited");
        newtodo.click();
        assertTasks("a edited");
        assertItemsLeft(1);
    }

    @Test
    public void testEditPressTabAtActiveFilter() {
        givenAtActive(ACTIVE, "a");

        startEdit("a", "a edited").pressTab();
        assertTasks("a edited");
        assertItemsLeft(1);
    }

    @Test
    public void testDeleteByEmptyingTaskTextAtActiveFilter() {
        givenAtActive(ACTIVE, "a");

        edit("a", "");
        assertNoVisibleTasks();
    }

    @Test
    public void testSwitchFromActiveToAll() {
        givenAtActive(aTask(ACTIVE, "a"), aTask(COMPLETED, "b"));

        filterAll();
        assertTasks("a", "b");
        assertItemsLeft(1);
    }

    @Test
    public void testCreateAtCompletedFilter() {
        givenAtCompleted(ACTIVE, "a");

        add("b");
        assertNoVisibleTasks();
        assertItemsLeft(2);
    }

    @Test
    public void testEditAtCompletedFilter() {
        givenAtCompleted(COMPLETED, "a", "b");

        edit("b", "b edited");
        assertTasks("a", "b edited");
        assertItemsLeft(0);
    }

    @Test
    public void testDeleteAtCompletedFilter() {
        givenAtCompleted(COMPLETED, "a");

        delete("a");
        assertNoTasks();
    }

    @Test
    public void testCancelEditAtCompletedFilter() {
        givenAtCompleted(COMPLETED, "a", "b");

        cancelEdit("b", "b edited");
        assertTasks("a", "b");
        assertItemsLeft(0);
    }

    @Test
    public void testCompleteAllAtCompletedFilter() {
        givenAtCompleted(aTask(ACTIVE, "a"), aTask(COMPLETED, "b"), aTask(ACTIVE, "c"));

        toggleAll();
        assertTasks("a", "b", "c");
        assertItemsLeft(0);
    }

    @Test
    public void testReopenAllAtCompletedFilter() {
        givenAtCompleted(COMPLETED, "a", "b");

        toggleAll();
        assertNoVisibleTasks();
        assertItemsLeft(2);
    }

    @Test
    public void testEditClickOutsideAtCompleteFilter() {
        givenAtCompleted(COMPLETED, "a", "b");

        startEdit("a", "a edited");
        newtodo.click();
        assertTasks("a edited", "b");
        assertItemsLeft(0);
    }

    @Test
    public void testEditPressTabAtCompletedFilter() {
        givenAtCompleted(COMPLETED, "a");

        startEdit("a", "a edited").pressTab();
        assertTasks("a edited");
        assertItemsLeft(0);
    }

    @Test
    public void testDeleteByEmptyingTaskTextAtCompletedFilter() {
        givenAtCompleted(COMPLETED, "a");

        edit("a", "");
        assertNoVisibleTasks();
    }

    @Test
    public void testSwitchFromCompletedToActive() {
        givenAtCompleted(aTask(ACTIVE, "a"), aTask(COMPLETED, "b"));

        filterActive();
        assertVisibleTasks("a");
        assertItemsLeft(1);
    }

    public ElementsCollection tasks = $$("#todo-list>li");
    public SelenideElement newtodo = $("#new-todo");

    @Step
    public void add(String... taskTexts) {
        for (String text : taskTexts) {
            newtodo.setValue(text).pressEnter();
        }
    }

    public SelenideElement startEdit(String oldTaskText, String newTaskText) {
        tasks.find(exactText(oldTaskText)).doubleClick();
        return tasks.find(cssClass("editing")).$(".edit").setValue(newTaskText);
    }

    @Step
    public void edit(String oldTaskText, String newTaskText) {
        startEdit(oldTaskText, newTaskText).pressEnter();
    }

    @Step
    public void cancelEdit(String oldTaskText, String newTaskText) {
        startEdit(oldTaskText, newTaskText).pressEscape();
    }

    @Step
    public void delete(String taskText) {
        tasks.find(exactText(taskText)).hover().$(".destroy").click();
    }

    @Step
    public void toggle(String taskText) {
        tasks.find(exactText(taskText)).$(".toggle").click();
    }

    @Step
    public void clearCompleted() {
        $("#clear-completed").click();
        $("#clear-completed").shouldNotBe(visible);
    }

    @Step
    public void toggleAll() {
        $("#toggle-all").click();
    }

    @Step
    public void assertTasks(String... taskTexts) {
        tasks.shouldHave(exactTexts(taskTexts));
    }

    @Step
    public void assertVisibleTasks(String... taskTexts) {
        tasks.filter(visible).shouldHave(exactTexts(taskTexts));
    }

    @Step
    public void assertNoTasks() {
        tasks.shouldBe(empty);
    }

    @Step
    public void assertNoVisibleTasks() {
        tasks.filter(visible).shouldBe(empty);
    }

    @Step
    public void filterAll() {
        $(By.linkText("All")).click();
    }

    @Step
    public void filterActive() {
        $(By.linkText("Active")).click();
    }

    @Step
    public void filterCompleted() {
        $(By.linkText("Completed")).click();
    }

    @Step
    public void assertItemsLeft(int count) {

        $("#todo-count>strong").shouldHave(exactText("" + count));
    }

    public void givenAtAll(Task... tasks) {
        ensurePageOpened();
        ArrayList<String> list = new ArrayList<String>();

        for (Task task : tasks) {
            list.add(task.toString());
        }

        String createTaskCommand = "localStorage.setItem(\"todos-troopjs\", \"[" + String.join(",", list) + "]\")";
        executeJavaScript(createTaskCommand);
        refresh();
    }

    public void givenAtActive(Task... tasks) {
        givenAtAll(tasks);
        filterActive();
    }

    public void givenAtCompleted(Task... tasks) {
        givenAtAll(tasks);
        filterCompleted();
    }

    public void givenAtAll(Status taskStatus, String... taskTexts) {
        givenAtAll(aTasks(taskStatus, taskTexts));
    }

    public void givenAtActive(Status taskStatus, String... taskTexts) {
        givenAtAll(aTasks(taskStatus, taskTexts));
        filterActive();
    }

    public void givenAtCompleted(Status taskStatus, String... taskTexts) {
        givenAtAll(aTasks(taskStatus, taskTexts));
        filterCompleted();
    }

    public Task aTask(Status taskStatus, String text) {
        return new Task(taskStatus, text);
    }

    public Task[] aTasks(Status taskStatus, String... text) {
        Task[] tasks = new Task[text.length];
        for (int i = 0; i < text.length; i++) {
            tasks[i] = aTask(taskStatus, text[i]);
        }
        return tasks;
    }

    public void ensurePageOpened() {
        if (!url().equals("https://todomvc4tasj.herokuapp.com/")) {
            open("https://todomvc4tasj.herokuapp.com/");
        }

    }

    public enum Status {
        ACTIVE("false"), COMPLETED("true");

        private String flag;

        Status(String flag) {
            this.flag = flag;
        }

        @Override
        public String toString() {
            return flag;
        }
    }

    public class Task {

        private String actualString = "";

        public Task(Status state, String text) {
            this.actualString = "{\\\"completed\\\":" + state + ", \\\"title\\\":\\\"" + text + "\\\"}";
        }

        @Override
        public String toString() {
            return actualString;
        }
    }

}