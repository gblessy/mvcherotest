package com.vladromanchenko.pagemodules;


import com.vladromanchenko.pagemodules.testConfigs.BaseTest;
import org.junit.Test;

import static com.vladromanchenko.pagemodules.pages.TodoMVCGivens.Status.*;
import static com.vladromanchenko.pagemodules.pages.TodoMVCGivens.givenAtAll;
import static com.vladromanchenko.pagemodules.pages.TodoMVC.*;

public class TodoMVCTest extends BaseTest {

     @Test
    public void testTasksCommonFlow() {
        givenAtAll(ACTIVE, "a");
        edit("a", "edited a");
        toggle("edited a");
        assertTasks("edited a");

        filterActive();
        assertNoVisibleTasks();

        add("b");
        assertVisibleTasks("b");

        //complete all
        toggleAll();
        assertNoVisibleTasks();

        filterCompleted();
        assertVisibleTasks("edited a", "b");
        //reopen
        toggle("b");
        assertVisibleTasks("edited a");

        clearCompleted();
        assertNoVisibleTasks();

        filterAll();
        assertVisibleTasks("b");

        delete("b");
        assertNoTasks();
    }
}