package com.vladromanchenko.pagemodules;

import com.vladromanchenko.pagemodules.testConfigs.BaseTest;
import org.junit.Test;


import static com.vladromanchenko.pagemodules.pages.TodoMVCGivens.aTask;
import static com.vladromanchenko.pagemodules.pages.TodoMVCGivens.aTasks;
import static com.vladromanchenko.pagemodules.pages.TodoMVCGivens.givenAtActive;
import static com.vladromanchenko.pagemodules.pages.TodoMVC.*;
import static com.vladromanchenko.pagemodules.pages.TodoMVCGivens.Status.*;


/**
 * Created by v.romanchenko on 4/23/2016.
 */
public class TodoMVCActiveFilterTest extends BaseTest {

    @Test
    public void testEditAtActiveFilter() {
        givenAtActive(ACTIVE, "a");

        edit("a", "a edited");
        assertVisibleTasks("a edited");
        assertItemsLeft(1);
    }

    @Test
    public void testDeleteAtActiveFilter() {
        givenAtActive(ACTIVE, "a","b");

        delete("b");
        assertTasks("a");
        assertItemsLeft(1);
    }

    @Test
    public void testCompleteAtActiveFilter() {
        givenAtActive(ACTIVE, "a", "b");

        toggle("a");
        assertVisibleTasks("b");
        assertItemsLeft(1);
    }

    @Test
    public void testClearCompletedAtActiveFilter() {
        givenAtActive(aTask(COMPLETED, "a"), aTask(ACTIVE, "b"));

        clearCompleted();
        assertVisibleTasks("b");
        assertItemsLeft(1);
    }

    @Test
    public void testReopenAllAtActiveFilter() {
        givenAtActive(COMPLETED, "a","b","c");

        toggleAll();
        assertTasks("a", "b", "c");
        assertItemsLeft(3);
    }

    @Test
    public void testCancelEditAtActiveFilter() {
        givenAtActive(ACTIVE, "a");

        cancelEdit("a", "a edited");
        assertVisibleTasks("a");
        assertItemsLeft(1);
    }

    @Test
    public void testEditClickOutsideAtActiveFilter() {
        givenAtActive(ACTIVE, "a");

        startEdit("a", "a edited");
        newtodo.click();
        assertTasks("a edited");
        assertItemsLeft(1);
    }

    @Test
    public void testEditPressTabAtActiveFilter() {
        givenAtActive(ACTIVE, "a");

        startEdit("a", "a edited").pressTab();
        assertTasks("a edited");
        assertItemsLeft(1);
    }

    @Test
    public void testDeleteByEmptyingTaskTextAtActiveFilter() {
        givenAtActive(ACTIVE, "a");

        edit("a", "");
        assertNoVisibleTasks();
    }

    @Test
    public void testSwitchFromActiveToAll() {
        givenAtActive(aTask(ACTIVE, "a"), aTask(COMPLETED,"b"));

        filterAll();
        assertTasks("a","b");
        assertItemsLeft(1);
    }
}
