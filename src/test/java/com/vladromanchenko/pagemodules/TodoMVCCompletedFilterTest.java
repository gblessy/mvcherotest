package com.vladromanchenko.pagemodules;

import com.vladromanchenko.pagemodules.testConfigs.BaseTest;
import org.junit.Test;

import static com.vladromanchenko.pagemodules.pages.TodoMVCGivens.*;
import static com.vladromanchenko.pagemodules.pages.TodoMVC.*;
import static com.vladromanchenko.pagemodules.pages.TodoMVCGivens.Status.*;

/**
 * Created by v.romanchenko on 4/23/2016.
 */
public class TodoMVCCompletedFilterTest extends BaseTest {
    @Test
    public void testCreateAtCompletedFilter() {
        givenAtCompleted(ACTIVE, "a");

        add("b");
        assertNoVisibleTasks();
        assertItemsLeft(2);
    }

    @Test
    public void testEditAtCompletedFilter() {
        givenAtCompleted(COMPLETED, "a","b");

        edit("b", "b edited");
        assertTasks("a","b edited");
        assertItemsLeft(0);
    }

    @Test
    public void testDeleteAtCompletedFilter() {
        givenAtCompleted(COMPLETED,"a");

        delete("a");
        assertNoTasks();
    }

    @Test
    public void testCancelEditAtCompletedFilter() {
        givenAtCompleted(COMPLETED,"a", "b");

        cancelEdit("b", "b edited");
        assertTasks("a","b");
        assertItemsLeft(0);
    }

    @Test
    public void testCompleteAllAtCompletedFilter() {
        givenAtCompleted(aTask(ACTIVE, "a"), aTask(COMPLETED, "b"), aTask(ACTIVE, "c"));

        toggleAll();
        assertTasks("a", "b", "c");
        assertItemsLeft(0);
    }

    @Test
    public void testReopenAllAtCompletedFilter() {
       givenAtCompleted(COMPLETED,"a", "b");

        toggleAll();
        assertNoVisibleTasks();
        assertItemsLeft(2);
    }

    @Test
    public void testEditClickOutsideAtCompleteFilter() {
        givenAtCompleted(COMPLETED,"a", "b");

        startEdit("a", "a edited");
        newtodo.click();
        assertTasks("a edited", "b");
        assertItemsLeft(0);
    }

    @Test
    public void testEditPressTabAtCompletedFilter() {
        givenAtCompleted(COMPLETED,"a");

        startEdit("a", "a edited").pressTab();
        assertTasks("a edited");
        assertItemsLeft(0);
    }

    @Test
    public void testDeleteByEmptyingTaskTextAtCompletedFilter() {
        givenAtCompleted(COMPLETED, "a");

        edit("a", "");
        assertNoVisibleTasks();
    }

    //Filtering
    @Test
    public void testSwitchFromCompletedToActive() {
        givenAtCompleted(aTask(ACTIVE,"a"), aTask(COMPLETED, "b"));

        filterActive();
        assertVisibleTasks("a");
        assertItemsLeft(1);
    }
}