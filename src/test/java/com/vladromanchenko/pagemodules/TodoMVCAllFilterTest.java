package com.vladromanchenko.pagemodules;

import com.vladromanchenko.pagemodules.testConfigs.BaseTest;
import org.junit.Test;

import static com.vladromanchenko.pagemodules.pages.TodoMVCGivens.*;
import static com.vladromanchenko.pagemodules.pages.TodoMVC.*;
import static com.vladromanchenko.pagemodules.pages.TodoMVCGivens.Status.*;

/**
 * Created by v.romanchenko on 4/23/2016.
 */
public class TodoMVCAllFilterTest extends BaseTest {

    @Test
    public void testCompleteAllAtAllFilter() {
        givenAtAll(ACTIVE, "a", "b", "c");

        toggleAll();
        assertTasks("a", "b", "c");
        assertItemsLeft(0);
    }

    @Test
    public void testClearCompletedAtAllFilter() {
        givenAtAll(COMPLETED, "a", "b", "c");

        clearCompleted();
        assertNoTasks();
    }

    @Test
    public void testReopenAtAllFilter() {
        givenAtAll(COMPLETED, "a", "b", "c");

        toggle("a");
        filterActive();
        assertVisibleTasks("a");
        assertItemsLeft(1);
    }

    @Test
    public void testReopenAllAtAllFilter() {
        givenAtAll(COMPLETED, "a");

        toggleAll();
        assertTasks("a");
        assertItemsLeft(1);
    }

    @Test
    public void testCancelEditAtAllFilter() {
        givenAtAll(aTask(ACTIVE, "a"), aTask(COMPLETED,"b"));

        cancelEdit("a", "a edited");
        assertTasks("a","b");
        assertItemsLeft(1);
    }

    @Test
    public void testEditClickOutsideAtAllFilter() {
        givenAtAll(aTask(ACTIVE, "a"), aTask(COMPLETED, "b"));

        startEdit("a", "a edited");
        newtodo.click();
        assertTasks("a edited", "b");
        assertItemsLeft(1);
    }

    @Test
    public void testEditPressTabAtAllFilter() {
        givenAtAll(aTask(ACTIVE, "a"), aTask(COMPLETED,"b"));

        startEdit("a", "a edited").pressTab();
        assertTasks("a edited", "b");
        assertItemsLeft(1);
    }

    @Test
    public void testDeleteByEmptyingTaskTextAtAllFilter() {
        givenAtAll(aTask(ACTIVE, "a"), aTask(COMPLETED, "b"));

        edit("a", "");
        assertTasks("b");
    }

        @Test
    public void testSwitchFromAllToCompletedFilter() {
        givenAtAll(aTask(COMPLETED, "a"), aTask(ACTIVE,"b"));

        filterCompleted();
        assertVisibleTasks("a");
        assertItemsLeft(1);
    }
}
